#pragma once
ref class ChromaKeyParams sealed
{
public:
	ChromaKeyParams();
	ChromaKeyParams::ChromaKeyParams(byte r, byte g, byte b, int tola = 65, int tolb = 75);

	property byte Red;
	property byte Green;
	property byte Blue;
	property int Tola;
	property int Tolb;
};


#pragma once
class CustomContext
{
public:
	CustomContext();
	CustomContext(Platform::Collections::Details::WFC::IVector<unsigned char>^ data);
	virtual ~CustomContext();

	ffmpeg::AVIOContext* GetAVIOContext() const { return this->_ioCtx; }
	void SeekToBegin() { _pos = 0; }
	unsigned int DataLength() const { return _data->Size; }

	ffmpeg::AVInputFormat* CustomContext::GetFormat();

private:
	const int BUFFER_SIZE = 32 * 1024;
	Platform::Collections::Details::WFC::IVector<unsigned char>^ _data;
	unsigned char* _buffer;
	int _pos;
	int _len;
	ffmpeg::AVIOContext* _ioCtx;
	ffmpeg::AVProbeData _probeData;
	ffmpeg::AVInputFormat* _AVInputFormat;

	static int read(void *opaque, uint8_t *buf, int buf_size);
	static int64_t seek(void *opaque, int64_t offset, int whence);

};


#include "pch.h"
#include "ffmpeg.h"

using Concurrency::critical_section;
using namespace ffmpeg;

Concurrency::critical_section FfmpegInitializer::g_init_cs;
bool FfmpegInitializer::g_lock_manager_registered = false;
bool FfmpegInitializer::g_av_codecs_registered = false;

extern "C" 
{ 
    int ffmpeg_lock_callback(void **mutex, enum AVLockOp op); 
} 

int ffmpeg_lock_callback(void **mutex, enum AVLockOp op) 
{ 
    switch(op) 
    { 
    case ffmpeg::AV_LOCK_CREATE: 
        { 
            *mutex = new critical_section; 
            break; 
        } 
    case AV_LOCK_OBTAIN: 
        { 
            ((critical_section*)(*mutex))->lock(); 
            break; 
        } 
    case AV_LOCK_RELEASE: 
        { 
            ((critical_section*)(*mutex))->unlock(); 
            break; 
        } 
    case AV_LOCK_DESTROY: 
        { 
             delete ((critical_section*)(*mutex));
            *mutex = NULL;
            break; 
        } 
    } 
    return 0; 
} 

void log(void* ptr, int level, const char* fmt, va_list vl) {
	std::array<char, 256> line;
	int printBuffer = 1;
	char buf[10240];
	memset(buf, 0, sizeof(buf)*sizeof(char));
	vsprintf(buf, fmt, vl);
	// av_log_format_line(ptr, level, fmt, vl, line.data(), line.size(), &printBuffer);
	OutputDebugStringA(buf);
}

void FfmpegInitializer::Init(bool isLog)
{
    // single init, one at a time, please
    AutoLock lock;

    if (!g_lock_manager_registered)
    {
        av_lockmgr_register(&ffmpeg_lock_callback);
        g_lock_manager_registered = true;
    }

    if (!g_av_codecs_registered)
    {
        ffmpeg::avcodec_register_all();
        ffmpeg::av_register_all();

        printf("License: %s\n",ffmpeg::avformat_license());
        printf("AVCodec version %d\n",ffmpeg::avformat_version());
        printf("AVFormat configuration: %s\n",ffmpeg::avformat_configuration());

        g_av_codecs_registered = true;
    }
	if (isLog)
		ffmpeg::av_log_set_callback(&log);	
}


void FfmpegInitializer::Lock()
{
    g_init_cs.lock();
}

void FfmpegInitializer::Unlock()
{
    g_init_cs.unlock();
}

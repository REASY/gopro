﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <robuffer.h>
#include "ffmpeg.h"
#include <queue>
#include <thread>

using namespace TestApp;

using namespace concurrency;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Web::Http;
using namespace Windows::Storage::Streams;
using namespace Windows::Security::Cryptography;
using namespace Windows::UI;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace Windows::Graphics::Imaging;
using namespace Windows::System::Threading;

template <typename T>
inline T clamp(const T& val, const T& minVal, const T& maxVal)
{
	return (val < minVal ? minVal : (val > maxVal ? maxVal : val));
}


MainPage::MainPage()
{
	InitializeComponent();
	this->DataContext = this;
	FfmpegInitializer::Init(false);
	_hashAlgo = Windows::Security::Cryptography::Core::HashAlgorithmProvider::OpenAlgorithm(Windows::Security::Cryptography::Core::HashAlgorithmNames::Md5);
	_logger = new DebugLogger();
	_logger->InfoLog(L"MainPage::MainPage()", 1, L"ASD");
	Application::Current->Suspending += ref new Windows::UI::Xaml::SuspendingEventHandler(this, &TestApp::MainPage::OnSuspending);
}
MainPage::~MainPage()
{
}
void SaveFrame(ffmpeg::AVFrame *pFrame, int width, int height, int iFrame, int idx, Platform::Array<unsigned char, 1>^ arr) {
	FILE *pFile;
	char szFilename[32];

	int i = idx;
	for (int y = 0; y < height; y++)
	{
		auto p = pFrame->data[0] + y*pFrame->linesize[0];
		for (int w = 0; w < width * 3; w++)
		{
			arr[i++] = (unsigned char)*(p + w);
		}
	}
	// Write pixel data
	/*for (y = 0; y<height; y++)
		fwrite(pFrame->data[0] + y*pFrame->linesize[0], 1, width * 3, pFile);*/

}

void MainPage::Page_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto filter = ref new Windows::Web::Http::Filters::HttpBaseProtocolFilter();
	filter->CacheControl->ReadBehavior = Windows::Web::Http::Filters::HttpCacheReadBehavior::MostRecent;
	this->_httpClient = ref new HttpClient(filter);
	_ChromaKeyParams = ref new ChromaKeyParams();
	ChromaKeyBrush = ref new SolidColorBrush(ColorHelper::FromArgb(_ChromaKeyParams->Tola, _ChromaKeyParams->Red, _ChromaKeyParams->Green, _ChromaKeyParams->Blue));
	RSlider->Value = _ChromaKeyParams->Red;
	GSlider->Value = _ChromaKeyParams->Green;
	BSlider->Value = _ChromaKeyParams->Blue;
	ASlider->Value = _ChromaKeyParams->Tola;
	_logger->InfoLog(L"MainPage::Page_Loaded()");
	_isStopped = true;
	_isStarted = false;
}

void MainPage::Btn_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	_backgroundImageResized = nullptr;
	_isStartedResize = false;
	this->img->Source = nullptr;
	/*create_task([this]
	{
		ThreadProc_Decode();
	});
	create_task([this]
	{
		ThreadProc_RefreshUI();
	});
	create_task([this]
	{
		ThreadGetTses(NULL);
	});*/
}
// For testing with files
//void MainPage::Btn_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
//{
//	this->img->Source = nullptr;
//	create_task([this]
//	{
//		Vector<unsigned char>^ vector = ref new Vector<unsigned char>();
//		create_task(Windows::Storage::ApplicationData::Current->LocalFolder->GetFileAsync("sample2.ts"), cancellation_token_source().get_token()).
//			then([this, vector](Windows::Storage::StorageFile^ file)
//		{
//			if (file != nullptr)
//			{
//				create_task(Windows::Storage::FileIO::ReadBufferAsync(file)).then([this, vector](IBuffer^ buffer)
//				{
//					Platform::Array<unsigned char, 1>^ arr = nullptr;
//					Platform::WriteOnlyArray<unsigned char, 1>^ w;
//					CryptographicBuffer::CopyToByteArray(buffer, &arr);
//					for (int i = 0; i < arr->Length; i++)
//						vector->Append(arr->get(i));
//					Process(vector);
//				});
//			}
//
//		}, task_continuation_context::use_current());
//	});
//}

task<IBuffer^> MainPage::ReadData(Windows::Storage::Streams::IBuffer^ buffer, Windows::Storage::Streams::IInputStream^ stream, Platform::Collections::Vector<unsigned char>^ result, cancellation_token ct)
{
	// Do an asynchronous read. We need to use use_current() with the continuations since the tasks are completed on
	// background threads and we need to run on the UI thread to update the UI.
	return create_task(
		stream->ReadAsync(buffer, buffer->Capacity, InputStreamOptions::ReadAhead),
		ct).then([=](task<IBuffer^> readTask)
	{
		IBuffer^ buffer = readTask.get();
		Platform::Array<unsigned char, 1>^ arr = nullptr;
		CryptographicBuffer::CopyToByteArray(buffer, &arr);
		for (unsigned int i = 0; i < arr->Length; i++)
			result->Append(arr->get(i));
		// Continue reading until the response is complete.  When done, return previousTask that is complete.
		return buffer->Length ? ReadData(buffer, stream, result, ct) : readTask;
	});
}

void MainPage::OnReadVideoFrameEvent(int pContainer)
{
	Container* c = (Container*)pContainer;
	auto pts = c->Packet->pts;
	auto isFound = _set.find(pts);
	if (isFound == _set.end())
	{
		_set.insert(pts);
		Container c(c->Packet, c->Frame);
		_pq.push(c);
	}
	else
	{
		ffmpeg::av_free(c->Frame->buf);
		ffmpeg::av_free(c->Frame);
		ffmpeg::av_free_packet(c->Packet);
		delete c->Packet;
	}
	_logger->InfoLog(L"ThreadProc_RefreshUI(). Deleted Container: 0x%08X", c);
	delete c;

}
DWORD WINAPI MainPage::ThreadProc_Decode(LPVOID param)
{
	DWORD threadId = GetCurrentThreadId();
	_logger->InfoLog(L"ThreadProc_Decode(). Decoding cycle begin. ThreadId: %d", threadId);
	int cnt = 0;
	while (!_decodeCts.get_token().is_canceled())
	{
		if (cnt % 15 == 0)
		{
			_logger->InfoLog(L"ThreadProc_Decode(). Data queue size is: %d", _tses.unsafe_size());
		}
		cnt++;

		Vector<unsigned char>^ vector;
		if (_tses.try_pop(vector))
		{
			_logger->InfoLog(L"ThreadProc_Decode(). Poped from _tses. Size: %d", vector->Size);
			GoProVideoDecoder^ decoder = ref new GoProVideoDecoder(vector);
			if (!decoder->Init(false))
			{
				_logger->ErrorLog(L"ThreadProc_Decode(). GoProVideoDecoder initialization failed!");
				continue;
			}
			auto cookie = decoder->OnReadVideoFrameEvent += ref new OnReadVideoFrameDelegate(this, &MainPage::OnReadVideoFrameEvent);
			decoder->Start();
			decoder->OnReadVideoFrameEvent -= cookie;
			decoder = nullptr;
			vector = nullptr;
			_logger->InfoLog(L"ThreadProc_Decode(). decoder = nullptr");
		}
		Sleep(20);
	}
	_logger->InfoLog(L"ThreadProc_Decode() Thread %d Exited", threadId);
	_decodeEvent.set();
	return 0;
}
DWORD WINAPI MainPage::ThreadProc_RefreshUI(LPVOID param)
{
	Sleep(3000);
	DWORD threadId = GetCurrentThreadId();
	_logger->InfoLog(L"ThreadProc_RefreshUI(). UI refresh cycle begin. ThreadId: %d", threadId);
	int cnt = 0;
	while (!_refreshUICts.get_token().is_canceled())
	{
		Container c;
		if (cnt % 15 == 0)
		{
			_logger->InfoLog(L"ThreadProc_RefreshUI(). Container priority-queue size is: %d", _pq.size());
		}
		cnt++;
		if (_pq.try_pop(c))
		{
			ffmpeg::AVPacket* packet = c.Packet;
			int width = c.Frame->width;
			int height = c.Frame->height;
			ffmpeg::AVPixelFormat pxlFmt = (ffmpeg::AVPixelFormat)c.Frame->format;

			_logger->InfoLog(L"ThreadProc_RefreshUI(). Packet pts: %lld", c.Packet->pts);

			auto newSize = ffmpeg::avpicture_get_size(ffmpeg::PIX_FMT_RGB24, width, height);
			uint8_t* buffer = (uint8_t*)ffmpeg::av_malloc(newSize*sizeof(uint8_t));
			assert(buffer != NULL);
			ffmpeg::AVFrame* pFrameRGB = ffmpeg::avcodec_alloc_frame();
			assert(pFrameRGB != NULL);
			ffmpeg::avpicture_fill((ffmpeg::AVPicture*)pFrameRGB, buffer, ffmpeg::PIX_FMT_RGB24, width, height);
			// Convert the image from its native format to RGB
			auto img_convert_ctx = ffmpeg::sws_getContext(width, height, pxlFmt, width, height,
				ffmpeg::PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
			assert(img_convert_ctx != NULL);
			ffmpeg::sws_scale(img_convert_ctx, c.Frame->data, c.Frame->linesize, 0, height, pFrameRGB->data, pFrameRGB->linesize);
			ffmpeg::sws_freeContext(img_convert_ctx);
			ffmpeg::av_frame_free(&pFrameRGB);

			_logger->InfoLog(L"ThreadProc_RefreshUI(). Deleted Frame: 0x%08X, Buffer: 0x%08X", c.Frame, c.Frame->buf);

			ffmpeg::av_free(c.Frame->buf);
			ffmpeg::av_frame_free(&c.Frame);

			// When we get the frame size, we need resize background image to need size
			if (IsChromaKeyOn && _backgroundImageResized == nullptr && _imageFile != nullptr && !_isStartedResize)
			{
				_isStartedResize = true;
				try
				{
					auto readTask = create_task(_imageFile->OpenAsync(FileAccessMode::Read));
					readTask.then([this, width, height](IRandomAccessStream^ stream)
					{
						_logger->InfoLog(L"storageFile->OpenAsync().then ThreadId: %d", GetCurrentThreadId());
						create_task(BitmapDecoder::CreateAsync(stream)).then([this, width, height](BitmapDecoder^ decoder)
						{
							_logger->InfoLog(L"BitmapDecoder::CreateAsync().then ThreadId: %d", GetCurrentThreadId());
							BitmapTransform^ transform = ref new BitmapTransform();
							transform->ScaledHeight = height;
							transform->ScaledWidth = width;
							transform->InterpolationMode = BitmapInterpolationMode::Cubic;
							create_task(decoder->GetPixelDataAsync(BitmapPixelFormat::Rgba8, BitmapAlphaMode::Straight, transform, ExifOrientationMode::IgnoreExifOrientation, ColorManagementMode::DoNotColorManage)).
								then([this](PixelDataProvider^ pixelProvider)
							{
								_backgroundImageResized = pixelProvider->DetachPixelData();
								_logger->InfoLog(L"BitmapDecoder decoded %d bytes", _backgroundImageResized->Length);
							});
						});
					});
				}
				catch (Exception^ ex)
				{
					_logger->ErrorLog(L"Can't read file %s: %s", _imageFile->Name, ex->Message->Begin());
				}

				//auto buffer = CryptographicBuffer::CreateFromByteArray(_backgroundImage);
				//InMemoryRandomAccessStream^ stream = ref new InMemoryRandomAccessStream();
				//DataWriter^ dataWriter = ref new DataWriter(stream);
				//dataWriter->WriteBuffer(buffer);
				//dataWriter->StoreAsync();
				//// when store is finished
				//dataWriter->FlushAsync();
				//stream->Seek(0);
				//
				//create_task(BitmapDecoder::CreateAsync(stream)).then([this, width, height](BitmapDecoder^ decoder)
				//{
				//	_logger->InfoLog(L"BitmapDecoder::CreateAsync().then ThreadId: %d", GetCurrentThreadId());
				//	/*BitmapTransform^ transform = ref new BitmapTransform();
				//	transform->InterpolationMode = BitmapInterpolationMode::Cubic;
				//	transform->ScaledHeight = height;
				//	transform->ScaledWidth = width;
				//	create_task(decoder->(BitmapPixelFormat::Rgba8, BitmapAlphaMode::Straight, transform, ExifOrientationMode::IgnoreExifOrientation, ColorManagementMode::DoNotColorManage)).
				//	then([this](PixelDataProvider^ pixelProvider)
				//	{
				//	_backgroundImageResized = pixelProvider->DetachPixelData();
				//	_logger->InfoLog(L"BitmapDecoder Resized decoded %d bytes", _backgroundImageResized->Length);
				//	});*/
				//});

			}

			this->Dispatcher->RunAsync(Windows::UI::Core::CoreDispatcherPriority::High,
				ref new Windows::UI::Core::DispatchedHandler([this, packet, buffer, width, height]
			{
				this->SetImage(packet, buffer, width, height);
			}));
			Sleep(20);
		}
		else
			Sleep(40);
	}
	_logger->InfoLog(L"ThreadProc_RefreshUI() Thread %d Exited", threadId);

	_refreshUIEvent.set();
	return 0;
}
int MainPage::rgb2cb(INT32 r, INT32 g, INT32 b)
{
	/*a utility function to convert colors in RGB into YCbCr*/
	INT32 cb;
	cb = (INT32)(128 + -0.168736*r - 0.331264*g + 0.5*b);
	return (cb);
}
//
int MainPage::rgb2cr(INT32 r, INT32 g, INT32 b)
{
	/*a utility function to convert colors in RGB into YCbCr*/
	INT32 cr;
	cr = (INT32)(128 + 0.5*r - 0.418688*g - 0.081312*b);
	return (cr);
}
double MainPage::colorclose(INT32 Cb_p, INT32 Cr_p, INT32 Cb_key, INT32 Cr_key, INT32 tola, INT32 tolb)
{
	/*decides if a color is close to the specified hue*/
	double temp = sqrt((Cb_key - Cb_p)*(Cb_key - Cb_p) + (Cr_key - Cr_p)*(Cr_key - Cr_p));
	if (temp < tola) { return (0.0); }
	if (temp < tolb) { return ((temp - tola) / (tolb - tola)); }
	return (1.0);
}

void MainPage::SetImage(ffmpeg::AVPacket* packet, uint8_t* buffer, int width, int height)
{
	INT32 cb, cr, r_bg, g_bg, b_bg;
	INT32 cb_key, cr_key;
	double mask;
	INT32 b_key = _ChromaKeyParams->Blue;
	INT32 g_key = _ChromaKeyParams->Green;
	INT32 r_key = _ChromaKeyParams->Red;

	Pts = packet->pts; 
	auto bmp = ref new Windows::UI::Xaml::Media::Imaging::WriteableBitmap(width, height);
	Platform::Array<unsigned char, 1>^ arr = nullptr;
	byte *pPixels = GetPointerToPixelData(bmp->PixelBuffer);
	uint8_t *p = buffer;
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			INT32 r = *p++; // R
			INT32 g = *p++; // G
			INT32 b = *p++; // B
			if (IsChromaKeyOn && _backgroundImageResized != nullptr)
			{
				INT32 chromaR, chromaG, chromaB;
				cb_key = rgb2cb(r_key, g_key, b_key);
				cr_key = rgb2cr(r_key, g_key, b_key);

				b_bg = _backgroundImageResized->Data[(x + y * width) * 4];
				g_bg = _backgroundImageResized->Data[(x + y * width) * 4 + 1];
				r_bg = _backgroundImageResized->Data[(x + y * width) * 4 + 3];
				cb = rgb2cb(r, g, b);
				cr = rgb2cr(r, g, b);
				mask = colorclose(cb, cr, cb_key, cr_key, _ChromaKeyParams->Tola, _ChromaKeyParams->Tolb);

				mask = 1 - mask;
				if (mask == 0.0)
				{
					chromaB = clamp(b, 0, 255);
					chromaG = clamp(g, 0, 255);
					chromaR = clamp(r, 0, 255);
				}
				else if (mask == 1.0)
				{
					chromaB = clamp(b_bg, 0, 255);
					chromaG = clamp(g_bg, 0, 255);
					chromaR = clamp(r_bg, 0, 255);
				}
				else
				{
					chromaB = clamp((INT32)(max(b - mask*b_key, 0) + mask*b_bg), 0, 255);
					chromaG = clamp((INT32)(max(g - mask*g_key, 0) + mask*g_bg), 0, 255);
					chromaR = clamp((INT32)(max(r - mask*r_key, 0) + mask*r_bg), 0, 255);
				}
				r = chromaR;
				g = chromaG;
				b = chromaB;
			}
			pPixels[(x + y * width) * 4] = b; // B
			pPixels[(x + y * width) * 4 + 1] = g; // G
			pPixels[(x + y * width) * 4 + 2] = r; // R
			pPixels[(x + y * width) * 4 + 3] = 0; // A

		}
	}
	_logger->InfoLog(L"SetImage(). Deleted Buffer: 0x%08X, Packet: 0x%08X", buffer, packet);	
	ffmpeg::av_free(buffer);
	ffmpeg::av_free_packet(packet);
	delete packet;
	img->Source = bmp;
	if ((int)border->Width != width)
	{
		border->Width = width;
		border->Height = height;
	}
}
DWORD WINAPI MainPage::ThreadGetTses(LPVOID param)
{
	DWORD threadId = GetCurrentThreadId();

	_logger->InfoLog(L"GetTses() Thread %d Started", threadId);

	std::vector<std::wstring> urls = {
		/*L"http://192.168.1.2:8080/bbb_360p_c.001",
		L"http://192.168.1.2:8080/bbb_360p_c.002",
		L"http://192.168.1.2:8080/bbb_360p_c.003",*/
		/*L"http://192.168.1.2:8080/2.ts",
		L"http://192.168.1.2:8080/3.ts",
		L"http://192.168.1.2:8080/4.ts",
		L"http://192.168.1.2:8080/5.ts",
		L"http://192.168.1.2:8080/6.ts",
		L"http://192.168.1.2:8080/7.ts",
		L"http://192.168.1.2:8080/8.ts",
		L"http://192.168.1.2:8080/9.ts",
		L"http://192.168.1.2:8080/10.ts",
		L"http://192.168.1.2:8080/11.ts",
		L"http://192.168.1.2:8080/12.ts",
		L"http://192.168.1.2:8080/13.ts",
		L"http://192.168.1.2:8080/14.ts",
		L"http://192.168.1.2:8080/15.ts",
		L"http://192.168.1.2:8080/16.ts",*/
		L"http://%s/live/amba_hls-1.ts",
		L"http://%s/live/amba_hls-2.ts",
		L"http://%s/live/amba_hls-3.ts",
		L"http://%s/live/amba_hls-4.ts",
		L"http://%s/live/amba_hls-5.ts",
		L"http://%s/live/amba_hls-6.ts",
		L"http://%s/live/amba_hls-7.ts",
		L"http://%s/live/amba_hls-8.ts",
		L"http://%s/live/amba_hls-9.ts",
		L"http://%s/live/amba_hls-10.ts",
		L"http://%s/live/amba_hls-11.ts",
		L"http://%s/live/amba_hls-12.ts",
		L"http://%s/live/amba_hls-13.ts",
		L"http://%s/live/amba_hls-14.ts",
		L"http://%s/live/amba_hls-15.ts",
		L"http://%s/live/amba_hls-16.ts",
	};
	int urlsLen = urls.size();
	wchar_t buf[512] = { 0 };
	for (int i = 0; i < urlsLen; i++)
	{
		swprintf_s(buf, sizeof(buf)/sizeof(wchar_t), urls[i].c_str(), _CameraHost->Begin());
		urls[i] = std::wstring(buf);
	}
	while (!_getTsesCts.get_token().is_canceled())
	{
		
		int k = 0;
		for (k = 0; k < urlsLen; k++)
		{
			if (_getTsesCts.get_token().is_canceled())
				break;
			Platform::String^ s = ref new Platform::String(urls[k].c_str());
			Uri^ resourceAddress = ref new Uri(s);
			HttpRequestMessage^ request = ref new HttpRequestMessage(HttpMethod::Get, resourceAddress);
			try
			{
				auto sendRequestTask = create_task(this->_httpClient->SendRequestAsync(request, HttpCompletionOption::ResponseHeadersRead), _getTsesCts.get_token());
				sendRequestTask.then([this](HttpResponseMessage^ response)
				{
					try
					{
						auto readStreamTask = create_task(response->Content->ReadAsInputStreamAsync(), _getTsesCts.get_token());
						return readStreamTask.then([this](IInputStream^ stream)
						{
							try
							{
								Vector<unsigned char>^ vector = ref new Vector<unsigned char>();
								IBuffer^ readBuffer = ref new Buffer(1 * 1024 * 1024);
								//IBuffer^ readBuffer = ref new Buffer(40 * 1024);
								auto readData = ReadData(readBuffer, stream, vector, _getTsesCts.get_token());
								return readData.then([this, vector](task<IBuffer^> previousTask)
								{
									try
									{
										// Check if any previous task threw an exception.
										previousTask.get();
										//rootPage->NotifyUser("Completed", NotifyType::StatusMessage);
									}
									catch (const task_canceled&)
									{
										_logger->WarnLog(L"Have got task_canceled");
										return;
										//rootPage->NotifyUser("Request canceled.", NotifyType::ErrorMessage);
									}
									catch (Exception^ ex)
									{
										_logger->WarnLog(L"Have got Exception: %s", ex->Message->Begin());
										return;
										//rootPage->NotifyUser("Error: " + ex->Message, NotifyType::ErrorMessage);
									}
									catch (...)
									{
										_logger->WarnLog(L"readData.then() Have got strange exception");
										return;
									}
									if (vector->Size != 0)
									{
										Process(vector);
									}
								});
								readBuffer = nullptr;
							}
							catch (...)
							{
								_logger->WarnLog(L"readStreamTask.then() Have got strange exception");
							}
						});
					}
					catch (...)
					{
						_logger->WarnLog(L"sendRequestTask.then() Have got strange exception");
					}
				});
				try { sendRequestTask.wait(); }
				catch (...)
				{
					_logger->WarnLog(L"GetTses() Catched some strange exception");
				}
			}
			catch (...)
			{
				_logger->WarnLog(L"GetTses() Inside loop. Catched some strange exception");
			}

			Sleep(40);
			/*_hashes.clear();
			_set.clear();*/
		}
	}
	_logger->InfoLog(L"GetTses() Thread %d Exited", threadId);
	_getTsesUIEvent.set();
	return 0;
}
void MainPage::Process(Vector<unsigned char>^ vector)
{
	Array<unsigned char>^ arr = ref new Array<unsigned char>(vector->Size);
	for (unsigned int i = 0; i < vector->Size; i++)
		arr->set(i, vector->GetAt(i));
	auto buffer = CryptographicBuffer::CreateFromByteArray(arr);
	auto buffHash = _hashAlgo->HashData(buffer);
	String ^ strHashBase64 = CryptographicBuffer::EncodeToBase64String(buffHash);

	if (_hashes.find(strHashBase64) == _hashes.end())
	{
		_hashes.insert(strHashBase64);
		_logger->InfoLog(L"Process(). Have got new Vector with size %d", vector->Size);
		_tses.push(vector);
	}
	else
	{
		_logger->InfoLog(L"Process(). Vector with size %d already in _tses", vector->Size);
	}
}

void MainPage::Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e)
{
	byte R, G, B, A;

	A = (byte)ASlider->Value;
	R = (byte)RSlider->Value;
	G = (byte)GSlider->Value;
	B = (byte)BSlider->Value;

	Color myColor = ColorHelper::FromArgb(A, R, G, B);
	showColor->Fill = ref  new SolidColorBrush(myColor);
}


void MainPage::ImageSelectionBtn_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{

	auto picker = ref new FileOpenPicker();
	picker->SuggestedStartLocation = PickerLocationId::PicturesLibrary;
	picker->ViewMode = PickerViewMode::Thumbnail;
	picker->FileTypeFilter->Append(".jpg");
	picker->FileTypeFilter->Append(".jpeg");
	picker->CommitButtonText = "Select background image";

	try
	{
		auto pickSingleFileTask = create_task(picker->PickSingleFileAsync());
		pickSingleFileTask.then([this](StorageFile^ file)
		{
			_imageFile = file;
			BackgroundImageName = file->Name;

			_logger->InfoLog(L"Selected background picture file: %s", file->Name->Begin());
		}).then([this, pickSingleFileTask]()
		{
			try
			{
				pickSingleFileTask.get();
			}
			catch (Exception^ ex)
			{
				_logger->ErrorLog(L"Inner. Picture selection was failed with error: %s", ex->Message->Begin());
				return;
			}
		});
	}
	catch (Exception^ ex)
	{
		_logger->ErrorLog(L"Outer. Picture selection was failed with error: %s", ex->Message->Begin());
		return;
	}


}
void MainPage::StartPreview()
{
	_decodeEvent.reset();
	_getTsesUIEvent.reset();
	_refreshUIEvent.reset();
	_backgroundImageResized = nullptr;
	_isStartedResize = false;
	this->img->Source = nullptr;
	_decodeCts = cancellation_token_source();
	_refreshUICts = cancellation_token_source();
	_getTsesCts = cancellation_token_source();
	
	_getTsesThread = ref new WorkItemHandler([this](IAsyncAction^ workItem)
	{
		ThreadGetTses(NULL);
	});
	
	_decodeThread = ref new WorkItemHandler([this](IAsyncAction^ workItem)
	{
		ThreadProc_Decode(NULL);
	});
	_refreshThread = ref new WorkItemHandler([this](IAsyncAction^ workItem)
	{
		ThreadProc_RefreshUI(NULL);
	});
	ThreadPool::RunAsync(_getTsesThread);
	ThreadPool::RunAsync(_decodeThread);
	ThreadPool::RunAsync(_refreshThread);

	_isStopped = false;
	_isStarted = true;
}
bool MainPage::CanStartPreview()
{
	return _isStopped;
}
void MainPage::StopPreview()
{
	_logger->InfoLog(L"Stopping preview...");
	_decodeCts.cancel();
	_getTsesCts.cancel();
	_refreshUICts.cancel();
	_decodeEvent.wait();
	_getTsesUIEvent.wait();
	_refreshUIEvent.wait();
	_isStopped = true;
	_isStarted = false;
	// Clear all containers and queues
	_set.clear();
	_hashes.clear();
	Container c;
	while (_pq.try_pop(c))
	{
		ffmpeg::av_free(c.Frame->buf);
		ffmpeg::av_frame_free(&c.Frame);
		ffmpeg::av_free_packet(c.Packet);
		delete c.Packet;
	}
	_pq.clear();
	_tses.clear();

	_logger->InfoLog(L"Stopped preview...");
}
bool MainPage::CanStopPreview()
{
	return _isStarted;
}
byte* MainPage::GetPointerToPixelData(Windows::Storage::Streams::IBuffer^ buffer)
{
	// Cast to Object^, then to its underlying IInspectable interface.
	Object^ obj = buffer;
	Microsoft::WRL::ComPtr<IInspectable> insp(reinterpret_cast<IInspectable*>(obj));

	// Query the IBufferByteAccess interface.
	Microsoft::WRL::ComPtr<Windows::Storage::Streams::IBufferByteAccess> bufferByteAccess;
	ThrowIfFailed(insp.As(&bufferByteAccess));

	// Retrieve the buffer data.
	byte* pixels = nullptr;
	ThrowIfFailed(bufferByteAccess->Buffer(&pixels));
	return pixels;
}
void MainPage::NotifyPropertyChanged(Platform::String^ prop)
{
	PropertyChangedEventArgs^ args =
		ref new PropertyChangedEventArgs(prop);
	PropertyChanged(this, args);
}

void MainPage::BtnStartPreview_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (CanStartPreview())
		StartPreview();
}


void MainPage::BtnStopPreview_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (CanStopPreview())
		StopPreview();
}


void TestApp::MainPage::OnSuspending(Platform::Object ^sender, Windows::ApplicationModel::SuspendingEventArgs ^e)
{
	_logger->InfoLog(L"OnSuspending()");
	if (CanStopPreview())
		StopPreview();
}

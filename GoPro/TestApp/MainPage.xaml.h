﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
#include "Container.h"
#include <robuffer.h>
#include "DebugLogger.h"
#include "ChromaKeyParams.h"
#include "BooleanToVisibilityConverter.h"
#include "DelegateCommand.h"

using namespace concurrency;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Web::Http;
using namespace Windows::Storage::Streams;
using namespace Windows::Security::Cryptography;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::Graphics::Imaging;
using namespace Windows::System::Threading;

namespace TestApp
{

	struct MyComparator
	{
		bool operator()(const Container& lhs, const Container& rhs) const
		{
			return lhs.Packet->pts > rhs.Packet->pts;
		}
	};

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::UI::Xaml::Data::Bindable]
	public ref class MainPage sealed : Windows::UI::Xaml::Data::INotifyPropertyChanged
	{
	public:
		MainPage();
		property bool IsChromaKeyOn
		{
			bool get(){ return _IsChromaKeyOn; }
			void set(bool value) { _IsChromaKeyOn = value; NotifyPropertyChanged("IsChromaKeyOn"); }
		}
		property int Pts
		{
			int get() { return _pts; }
			void set(int value) { _pts = value; NotifyPropertyChanged("Pts"); }
		}
		property SolidColorBrush^ ChromaKeyBrush
		{
			SolidColorBrush^ get() { return _ChromaKeyBrush; }
			void set(SolidColorBrush^ value)
			{ 
				_ChromaKeyBrush = value; 
				_ChromaKeyParams = ref new ChromaKeyParams(_ChromaKeyBrush->Color.R, _ChromaKeyBrush->Color.G, _ChromaKeyBrush->Color.B);
				_ChromaKeyParams->Tola = _ChromaKeyBrush->Color.A;
				NotifyPropertyChanged("ChromaKeyBrush"); 
			}
		}
		property String^ BackgroundImageName
		{
			String^ get() { return _BackgroundImageName; }
			void set(String^ value)
			{
				_BackgroundImageName = value;
				NotifyPropertyChanged("BackgroundImageName");
			}
		}
		property String^ CameraHost
		{
			String^ get() { return _CameraHost; }
			void set(String^ value)
			{
				_CameraHost = value;
				NotifyPropertyChanged("CameraHost");
			}
		}
		property String^ Password
		{
			String^ get() { return _Password; }
			void set(String^ value)
			{
				_Password = value;
				NotifyPropertyChanged("Password");
			}
		}
		virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;

	private:
		~MainPage();
		// UI
		volatile int _pts;
		volatile bool _isStarted;
		volatile bool _isStopped;
		DelegateCommand^ _StartPreviewCommand;
		DelegateCommand^ _StopPreviewCommand;
		bool _IsChromaKeyOn;
		SolidColorBrush^ _ChromaKeyBrush;
		ChromaKeyParams^ _ChromaKeyParams;
		String^ _BackgroundImageName;
		String^ _CameraHost;
		String^ _Password;
		StorageFile^ _imageFile;
		// Platform::Array<unsigned char>^ _backgroundImage;
		
		Platform::Array<unsigned char>^ _backgroundImageResized;
		volatile bool _isStartedResize;
		HttpClient^ _httpClient;
		Windows::Security::Cryptography::Core::HashAlgorithmProvider^ _hashAlgo;
		cancellation_token_source _decodeCts;
		cancellation_token_source _refreshUICts;
		cancellation_token_source _getTsesCts;
		concurrency::event _decodeEvent;
		concurrency::event _refreshUIEvent;
		concurrency::event _getTsesUIEvent;
		concurrent_unordered_set<int64_t> _set;
		concurrent_unordered_set<String^> _hashes;
		concurrent_priority_queue<Container, MyComparator> _pq;
		concurrent_queue<Vector<unsigned char>^> _tses;
		ILogger* _logger;
		WorkItemHandler^ _getTsesThread;
		WorkItemHandler^ _decodeThread;
		WorkItemHandler^ _refreshThread;
		virtual void NotifyPropertyChanged(Platform::String^ prop);
		void Page_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void StartPreview();
		bool CanStartPreview();
		void StopPreview();
		bool CanStopPreview();
		task<IBuffer^> ReadData(IBuffer^ buffer, IInputStream^ stream, Vector<unsigned char>^ result, cancellation_token ct);

		static inline void ThrowIfFailed(HRESULT hr)
		{
			if (FAILED(hr))
			{
				throw Exception::CreateException(hr);
			}
		}

		byte* GetPointerToPixelData(Windows::Storage::Streams::IBuffer^ buffer);		
		void Btn_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void OnReadVideoFrameEvent(int pContainer);
		DWORD WINAPI ThreadProc_Decode(LPVOID param);
		DWORD WINAPI ThreadProc_RefreshUI(LPVOID param);
		DWORD WINAPI ThreadGetTses(LPVOID param);
		void SetImage(ffmpeg::AVPacket* packet, uint8_t* buffer, int width, int height);
		void Process(Vector<unsigned char>^ vector);
		void Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e);
		void ImageSelectionBtn_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		
		int rgb2cr(INT32 r, INT32 g, INT32 b);
		int rgb2cb(INT32 r, INT32 g, INT32 b);
		double colorclose(INT32 Cb_p, INT32 Cr_p, INT32 Cb_key, INT32 Cr_key, INT32 tola, INT32 tolb);
		void BtnStartPreview_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void BtnStopPreview_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void OnSuspending(Platform::Object ^sender, Windows::ApplicationModel::SuspendingEventArgs ^e);
};
}

#pragma once

class ILogger
{
public:
	// Empty virtual destructor for proper cleanup
	virtual ~ILogger() {}

	virtual void ErrorLog(const wchar_t* format, ...) = 0;
	virtual void WarnLog(const wchar_t* format, ...) = 0;
	virtual void InfoLog(const wchar_t* format, ...) = 0;
};
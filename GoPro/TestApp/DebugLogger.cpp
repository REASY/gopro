#include "pch.h"
#include "DebugLogger.h"

DebugLogger::DebugLogger()
{
}

DebugLogger::~DebugLogger()
{
}
void DebugLogger::ErrorLog(const wchar_t* format, ...)
{
	va_list args;
	va_start(args, format);
	Log(L"ERR", format, args);
	va_end(args);
}
void DebugLogger::InfoLog(const wchar_t* format, ...)
{
	va_list args;
	va_start(args, format);
	Log(L"INFO", format, args);
	va_end(args);
}
void DebugLogger::WarnLog(const wchar_t* format, ...)
{
	va_list args;
	va_start(args, format);
	Log(L"WARN", format, args);
	va_end(args);
}

void DebugLogger::Log(const std::wstring& type, const wchar_t* format, va_list args)
{
	wchar_t buf[1024];
	vswprintf(buf, sizeof(buf) / sizeof(wchar_t), format, args);
	DWORD threadId = GetCurrentThreadId();
	DWORD procId = GetCurrentProcessId();
	wchar_t result[2048];
	swprintf(result, sizeof(result) / sizeof(wchar_t), L"%s [%s] %ld %ld %s\r\n", CurrentDateTime().c_str(), type.c_str(), procId, threadId, buf);
	OutputDebugStringW(result);
}
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::wstring DebugLogger::CurrentDateTime()
{
	using namespace std::chrono;

	high_resolution_clock::time_point p = high_resolution_clock::now();
	milliseconds ms = duration_cast<milliseconds>(p.time_since_epoch());

	seconds s = duration_cast<seconds>(ms);
	time_t     now = s.count();
	struct tm  tstruct;
	wchar_t       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	wcsftime(buf, sizeof(buf) / sizeof(wchar_t), L"%Y-%m-%d %X", &tstruct);
	std::wstring bufRes;
	bufRes.append(buf);
	swprintf(buf, L".%03ld", ms.count() % 1000);
	bufRes.append(buf);

	return bufRes;
}
#pragma once

namespace UI
{
	namespace Convertors
	{
		public ref class BooleanToVisibilityConverter sealed : Windows::UI::Xaml::Data::IValueConverter
		{
		public:
			BooleanToVisibilityConverter() {}
			virtual ~BooleanToVisibilityConverter(){}

			virtual Platform::Object^ Convert(
				Platform::Object^ value,
				Windows::UI::Xaml::Interop::TypeName targetType,
				Platform::Object^ parameter,
				Platform::String^ language);

			virtual Platform::Object^ ConvertBack(
				Platform::Object^ value,
				Windows::UI::Xaml::Interop::TypeName targetType,
				Platform::Object^ parameter,
				Platform::String^ language);
		};
	}
}

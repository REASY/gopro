#include "pch.h"
#include "ChromaKeyParams.h"


ChromaKeyParams::ChromaKeyParams()
{
	Red = 0;
	Green = 183;
	Blue = 76;
	Tola = 65;
	Tolb = 75;
}
ChromaKeyParams::ChromaKeyParams(byte r, byte g, byte b, int tola, int tolb)
{
	Red = r;
	Green = g;
	Blue = b;
	Tola = tola;
	Tolb = tolb;
}
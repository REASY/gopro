#include "pch.h"
#include "CustomContext.h"

const int SEEK_SUCCED = 0;
const int SEEK_FAILED = -1;

CustomContext::CustomContext(Platform::Collections::Details::WFC::IVector<unsigned char>^ data)
	:_data(nullptr), _pos(0), _AVInputFormat(NULL)
{
	_data = ref new Platform::Collections::Vector<unsigned char>();
	for (int i = 0; i < data->Size; i++)
		_data->Append(data->GetAt(i));
	_buffer = (unsigned char*)ffmpeg::av_malloc(CustomContext::BUFFER_SIZE);
	assert(_buffer != NULL);
	memset(_buffer, 0, CustomContext::BUFFER_SIZE);
	_len = _data->Size;
	_ioCtx = ffmpeg::avio_alloc_context(_buffer, CustomContext::BUFFER_SIZE, 0, (void*)this, CustomContext::read, NULL, CustomContext::seek);
	assert(_ioCtx != NULL);
	_ioCtx->seekable = 1;
	_ioCtx->write_flag = 0;
	_pos = 0;
	this->_AVInputFormat = this->GetFormat();
}
CustomContext::~CustomContext()
{
	ffmpeg::av_free(_buffer);
	ffmpeg::av_free(_ioCtx);
	_data = nullptr;
}
ffmpeg::AVInputFormat* CustomContext::GetFormat() 
{
	if (this->_AVInputFormat == NULL)
	{
		auto currPos = _pos;
		auto ulReadBytes = read(this, _buffer, CustomContext::BUFFER_SIZE);
		_probeData.buf = _buffer;
		_probeData.buf_size = ulReadBytes;
		_probeData.filename = "";
		_probeData.mime_type = "";
		// Determine the input-format:
		this->_AVInputFormat = ffmpeg::av_probe_input_format(&_probeData, 1);
		_pos = currPos;
	}
	return this->_AVInputFormat;
}
int CustomContext::read(void *opaque, uint8_t *buf, int buf_size)
{
	CustomContext* cc = static_cast<CustomContext*>(opaque);
	if (cc->_pos == cc->_len)
		return AVERROR_EOF;
	memset(buf, 0, buf_size);
	int bytesRead = 0;
	int i = 0;
	int len = cc->_len;
	for (bytesRead = 0, i = cc->_pos; i < len && bytesRead < buf_size; i++, bytesRead++)
	{
		buf[bytesRead] = cc->_data->GetAt(i);
	}
	cc->_pos = i;
	return bytesRead;
}
int64_t CustomContext::seek(void *opaque, int64_t offset, int whence)
{
	CustomContext* cc = static_cast<CustomContext*>(opaque);
	auto len = cc->_len;
	auto pos = cc->_pos;

	if (whence == AVSEEK_SIZE)
		return len;
	int64_t res = SEEK_FAILED;
	switch (whence)
	{
	case SEEK_SET:
		if (offset < len)
		{
			cc->_pos = offset;
			res = SEEK_SUCCED;
		}
		break;
	case SEEK_CUR:
		if (pos + offset < len)
		{
			cc->_pos = pos + offset;
			res = SEEK_SUCCED;
		}
		break;
	case SEEK_END:
		// assert(false, "Got SEEK_END!!");
		break;
	default:
		break;
	}
	return res;
}
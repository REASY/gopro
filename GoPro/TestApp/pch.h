﻿//
// pch.h
// Header for standard system include files.
//

#pragma once

#include <collection.h>
#include <ppltasks.h>
#include <mutex>
#include <thread> 
#include <vector>
#include <iostream>

#include <limits.h>
#include <stdint.h>

#include <ppl.h>
#include <concurrent_priority_queue.h>
#include <concurrent_queue.h>
#include <concurrent_unordered_set.h>

namespace ffmpeg {
	extern "C" {

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/mathematics.h>
#include <libavutil/audioconvert.h>
		//#include "libavformat/riff.h"
		//#include "libavformat/metadata.h"
		//#include "libavformat/utils.h"
		//#include "libavcodec/opt.h"
#include <libavutil/rational.h>
		//#include "options.h"
#include <libavutil/avstring.h>
		//#include "libavutil/internal.h"
#include <libswscale/swscale.h>
	}
}

#include "FastDelegate.h"
#include "FastDelegateBind.h"
#include "App.xaml.h"
#include "GoProVideoDecoder.h"

//#include <robuffer.h>

#pragma once
using namespace Platform;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::Foundation;

delegate void ExecuteDelegate(Object^ parameter);
delegate bool CanExecuteDelegate(Object^ parameter);

ref class DelegateCommand sealed : public ICommand
{
	ExecuteDelegate^ _executeDelegate;
	CanExecuteDelegate^ _canExecuteDelegate;

public:
	DelegateCommand(ExecuteDelegate^ executeDelegate,
		CanExecuteDelegate^ canExecuteDelegate = nullptr)
	{
		_executeDelegate = executeDelegate;
		_canExecuteDelegate = canExecuteDelegate;
	}

	virtual event EventHandler<Object^>^ CanExecuteChanged;

	virtual void Execute(Object^ parameter)
	{
		_executeDelegate(parameter);
	}

	virtual bool CanExecute(Object^ parameter)
	{
		return _canExecuteDelegate == nullptr || _canExecuteDelegate(parameter);
	}
};


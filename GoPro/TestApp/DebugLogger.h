#pragma once
#include "ILogger.h"
class DebugLogger :
	public ILogger
{
public:
	DebugLogger();
	~DebugLogger();

	virtual void ErrorLog(const wchar_t* format, ...);
	virtual void InfoLog(const wchar_t* format, ...);
	virtual void WarnLog(const wchar_t* format, ...);

private:
	void Log(const std::wstring& type, const wchar_t* format, va_list args);
	// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
	const std::wstring CurrentDateTime();
};


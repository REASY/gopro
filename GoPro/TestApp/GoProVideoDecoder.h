#pragma once
#include "CustomContext.h"
#include "ILogger.h"
#include "DebugLogger.h"

delegate void OnReadVideoFrameDelegate(int pContainer);

ref class GoProVideoDecoder sealed
{
public:
	GoProVideoDecoder(Platform::Collections::Details::WFC::IVector<unsigned char>^ data);
	virtual ~GoProVideoDecoder();

	bool Init(bool processAudio);
	event OnReadVideoFrameDelegate^ OnReadVideoFrameEvent;
	int GetVideoWidth();
	int GetVideoHeight();
	void Start();

	bool HasAudio() { return _audioStreamIndex != -1; }
	bool HasVideo() { return _videoStreamIndex != -1; }

	

private:
	Platform::Collections::Details::WFC::IVector<unsigned char>^ _data;
	CustomContext* _customContext;
	ffmpeg::AVFormatContext* _pAVFormatContext;
	bool _processAudio;
	int _videoStreamIndex;
	int _audioStreamIndex;
	ffmpeg::AVCodecContext  *_pVideoCodecCtx;
	ffmpeg::AVCodec         *_pVideoCodecTemplate;
	ffmpeg::AVCodecContext  *_pAudioCodecCtx;
	ffmpeg::AVCodec         *_pAudioCodecTemplate;
	ffmpeg::AVFrame         *_pVideoFrame;
	ffmpeg::AVFrame         *_pAudioFrame;

	bool GoProVideoDecoder::OnReadAudioPacket(ffmpeg::AVPacket* packet);
	bool GoProVideoDecoder::OnReadVideoPacket(ffmpeg::AVPacket* packet);
	ILogger* const _logger;
};


#include "pch.h"
#include "GoProVideoDecoder.h"
#include "ffmpeg.h"
#include "Container.h"


GoProVideoDecoder::GoProVideoDecoder(Platform::Collections::Details::WFC::IVector<unsigned char>^ data)
	:_data(data), _customContext(nullptr), _pAVFormatContext(NULL), _pVideoCodecCtx(NULL), _pVideoFrame(NULL), _pAudioFrame(NULL), _logger(new DebugLogger())
{
	if (data->Size == 0)
		throw std::exception("data size can't be 0");
	_customContext = new CustomContext(data);
}
GoProVideoDecoder::~GoProVideoDecoder()
{
	if (_customContext != nullptr)
	{
		delete _customContext;
		_customContext = nullptr;
	}
	if (_pVideoCodecCtx != NULL)
	{
		ffmpeg::avcodec_close(_pVideoCodecCtx);
		_pVideoCodecCtx = NULL;
	}
	if (_pAudioCodecCtx != NULL)
	{
		ffmpeg::avcodec_close(_pAudioCodecCtx);
		_pAudioCodecCtx = NULL;
	}
	if (_pVideoFrame != NULL)
	{
		ffmpeg::av_frame_free(&_pVideoFrame);
		_pVideoFrame = NULL;
	}
	if (_pAudioFrame != NULL)
	{
		ffmpeg::av_frame_free(&_pAudioFrame);
		_pAudioFrame = NULL;
	}
	if (_pAVFormatContext != NULL)
	{
		//ffmpeg::avformat_close
		ffmpeg::avformat_close_input(&_pAVFormatContext);
		_pAVFormatContext = NULL;
	}
	_data = nullptr;
	delete _logger;
}

bool GoProVideoDecoder::Init(bool processAudio)
{
	_logger->InfoLog(L"GoProVideoDecoder::Init()");
	if (_customContext->DataLength() <= 0)
		return false;
	if (_customContext->GetFormat() == NULL)
	{
		_logger->WarnLog(L"GoProVideoDecoder::Init(). Initialization failed, can't get format.");
		return false;
	}
	_processAudio = processAudio;

	FfmpegInitializer::AutoLock lock;

	_pAVFormatContext = ffmpeg::avformat_alloc_context();
	if (_pAVFormatContext == NULL)
		return false;
	_pAVFormatContext->pb = _customContext->GetAVIOContext();
	_pAVFormatContext->flags = AVFMT_FLAG_CUSTOM_IO;
	// Determine the input-format:
	_pAVFormatContext->iformat = _customContext->GetFormat();

	// Open video file
	if (ffmpeg::avformat_open_input(&_pAVFormatContext, "" /*filename*/, NULL, NULL /*&format_opts*/) != 0)
		return false; // Couldn't open file
	//_customContext->SeekToBegin();
	// Retrieve stream information
	if (ffmpeg::avformat_find_stream_info(_pAVFormatContext, NULL) < 0)
	{
		ffmpeg::avformat_free_context(_pAVFormatContext);
		_pAVFormatContext = NULL;
		return false; // Couldn't find stream information
	}

	// Dump information about file onto standard error
	ffmpeg::av_dump_format(_pAVFormatContext, 0, "CustomContext", false);

	_videoStreamIndex = -1;
	_audioStreamIndex = -1;
	for (unsigned i = 0; i < _pAVFormatContext->nb_streams; i++)
	{
		if (_videoStreamIndex == -1 && _pAVFormatContext->streams[i]->codec->codec_type == ffmpeg::AVMEDIA_TYPE_VIDEO)
		{
			// remember contexts
			_videoStreamIndex = i;
			_pVideoCodecCtx = _pAVFormatContext->streams[_videoStreamIndex]->codec;
			// Find the decoder for the video stream   
			_pVideoCodecTemplate = ffmpeg::avcodec_find_decoder(_pAVFormatContext->streams[_videoStreamIndex]->codec->codec_id);
			if (_pVideoCodecTemplate == NULL)
				return false; // video codec not found
			// Open codec
			if (avcodec_open2(_pVideoCodecCtx, _pVideoCodecTemplate, NULL) < 0)
				return false; // Could not open codec
		}
		if (processAudio && _audioStreamIndex == -1 && _pAVFormatContext->streams[i]->codec->codec_type == ffmpeg::AVMEDIA_TYPE_AUDIO)
		{
			_audioStreamIndex = i;
			// Find the decoder for the audio stream   
			_pAudioCodecTemplate = ffmpeg::avcodec_find_decoder(_pAVFormatContext->streams[_audioStreamIndex]->codec->codec_id);
			if (_pAudioCodecTemplate == NULL)
				return false; // audio codec not found
			_pAudioCodecCtx = _pAVFormatContext->streams[_audioStreamIndex]->codec;
			// Open codec
			if (avcodec_open2(_pAudioCodecCtx, _pAudioCodecTemplate, NULL) < 0)
				return false; // Could not open codec
		}
	}
	if (_videoStreamIndex == -1)
		return false;
	_pVideoFrame = ffmpeg::avcodec_alloc_frame();
	if (_pVideoFrame == NULL)
		return false;
	_pAudioFrame = ffmpeg::avcodec_alloc_frame();
	if (_pAudioFrame == NULL)
		return false;
	_logger->InfoLog(L"GoProVideoDecoder::Init() end");
	return true;
}
void GoProVideoDecoder::Start()
{
	_logger->InfoLog(L"GoProVideoDecoder::Start()");
	unsigned int cntFrames = 0;
	unsigned int cntPictures = 0;

	bool done = false;
	bool done_video = false;
	bool done_audio = (_processAudio && HasAudio()) ? false : true;
	while (true)
	{
		ffmpeg::AVPacket packet;
		ffmpeg::av_init_packet(&packet);
		packet.data = NULL;
		packet.size = 0;

		// Read a frame
		if (av_read_frame(_pAVFormatContext, &packet) < 0)
		{
			ffmpeg::av_free_packet(&packet);      // Free the packet that was allocated by av_read_frame

			break;                             // Frame read failed (e.g. end of stream)
		}
		//printf("Packet of stream %d, size %d\n",packet.stream_index, packet.size);
		if (_processAudio && packet.stream_index == _audioStreamIndex)
		{
			done_audio = OnReadAudioPacket(&packet);
		}
		if (packet.stream_index == _videoStreamIndex)
		{
			done_video = OnReadVideoPacket(&packet);
			if (done_video)
				cntPictures++;
		}  // stream_index==videoStream
		ffmpeg::av_free_packet(&packet);      // Free the packet that was allocated by av_read_frame
		done = done_audio && done_video;
		cntFrames++;
	}
	_logger->InfoLog(L"GoProVideoDecoder::Start() end. Have read %d frames. Have got %d pictures", cntFrames, cntPictures);
}
int GoProVideoDecoder::GetVideoWidth()
{
	return _pVideoCodecCtx == NULL ? -1 : _pVideoCodecCtx->width;
}
int GoProVideoDecoder::GetVideoHeight()
{
	return _pVideoCodecCtx == NULL ? -1 : _pVideoCodecCtx->height;
}

bool GoProVideoDecoder::OnReadVideoPacket(ffmpeg::AVPacket* packet)
{
	// Is this a packet from the video stream -> decode video frame
	int got_picture = 0;

	auto ret = avcodec_decode_video2(_pVideoCodecCtx, _pVideoFrame, &got_picture, packet);
	if (ret < 0)
	{
		char buf[1024];
		char* p = ffmpeg::av_make_error_string(buf, sizeof(buf), ret);
		int wchars_num = MultiByteToWideChar(CP_UTF8, 0, p, -1, NULL, 0);
		_logger->InfoLog(L"GoProVideoDecoder::OnReadVideoPacket(). avcodec_decode_video2() failed. Error: %d", ret);
		return false;
	}
	// Did we get a video picture?
	if (got_picture)
	{
		//_posVect.push_back(packet->pts);
		// update current frame counters and tracking time
		ffmpeg::AVRational millisecondbase = { 1, 1000 };
		int64_t f = packet->dts;
		int64_t time_milliseconds = ffmpeg::av_rescale_q(packet->dts, _pAVFormatContext->streams[_videoStreamIndex]->time_base, millisecondbase);

		ffmpeg::AVFrame* frame = ffmpeg::avcodec_alloc_frame();
		assert(frame != NULL);
		assert(ffmpeg::av_frame_ref(frame, _pVideoFrame) ==0);
		

		ffmpeg::AVPacket* packetCopy = new ffmpeg::AVPacket;
		ffmpeg::av_init_packet(packetCopy);
		ffmpeg::av_copy_packet(packetCopy, packet);

		Container* c = new Container();
		c->Packet = packetCopy;
		c->Frame = frame;
		_logger->InfoLog(L"GoProVideoDecoder::OnReadVideoPacket() allocated Container: 0x%08X, Frame: 0x%08X, Buffer: 0x%08X", c, frame, frame->buf);
		OnReadVideoFrameEvent((int)c);
		// let the consumer know that we have reached the desired video frame
		return true;
	}
	// not enough packets or wrong frame
	// need to read more, send more packets!
	return false;
}

bool GoProVideoDecoder::OnReadAudioPacket(ffmpeg::AVPacket* packet)
{
	int got_frame = 0;
	int len = avcodec_decode_audio4(_pAudioCodecCtx, _pAudioFrame, &got_frame, packet);
	if (got_frame)
	{
		/* if a frame has been decoded, output it */
		//int data_size = av_samples_get_buffer_size(NULL, c->channels,
		//    decoded_frame->nb_samples,
		//    c->sample_fmt, 1);
		//fwrite(decoded_frame->data[0], 1, data_size, outfile);
		//OnReadAudioFrameEvent(_pVideoFrame, packet);
		// got the frame
		return true;
	}
	// send more packets
	return false;
}